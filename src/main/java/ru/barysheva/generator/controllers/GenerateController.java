package ru.barysheva.generator.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.barysheva.generator.service.NumberService;


@RestController
@RequestMapping("/number")
public class GenerateController {

    @Autowired
    private NumberService numberService;

    @GetMapping("/random")
    public String randomGenerator() {
        return numberService.random();
    }

    @GetMapping("/next")
    public String nextGenerator() {
        return numberService.next();
    }
}

