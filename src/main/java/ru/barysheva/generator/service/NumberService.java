package ru.barysheva.generator.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Random;

@Service
public class NumberService {
    private String numberForNext = "";
    private String letterForNext = "";

    private final String[] letters = {"А", "Е", "Т", "О", "Р", "Н", "У", "К", "Х", "С", "В", "М"};
    private final String REGION = " 116 RUS";

    /**
     * Метод вычисляет рандомный номер машины в формате А111АА 116 RUS
     * Пример рандомный: C399BA 116 RUS
     */
    public String random() {
        numberForNext = randomNumber();
        letterForNext = randomLetter();
        StringBuilder result = new StringBuilder(letterForNext).insert(1, numberForNext);
        return result + REGION;
    }

    /**
     * Метод вычисляет номер машины следующий за текущим, буквенное обозначение меняется когда
     * цифры номера машины пересекают порог 999 - в этом случае начинается перебор букв начиная с последней
     * Пример последовательный 1: после номера C399BA 116 RUS должен идти номер C400BA 116 RUS
     * Пример последовательный 2: после номера C999BA 116 RUS должен идти номер C000BB 116 RUS
     */
    public String next() {

        if (letterForNext == "" & numberForNext == "") {
            letterForNext = randomLetter();
            numberForNext = randomNumber();

            StringBuilder result = new StringBuilder(letterForNext).insert(1, numberForNext);
            return result + REGION;

        } else {
            numberForNext = formatNumber(numberForNext);
            if (numberForNext.equals("000")) {
                letterForNext = formatThirdLetter(letterForNext);
            }
            StringBuilder result = new StringBuilder(formatThirdLetter(letterForNext)).insert(1, numberForNext);
            return result + REGION;
        }

    }

    /**
     * Метод вычисляет рандомные буквы в формате ААА
     */
    private String randomLetter() {
        int i = 0;
        String strLetter = "";

        while (i < 3) {
            int n = (int) Math.floor(Math.random() * letters.length);
            strLetter += letters[n];
            i++;
        }
        return strLetter;
    }

    /**
     * Метод вычисляет рандомные цифры в формате 111
     */
    private String randomNumber() {
        int i = 0;
        String strNumber = "";
        Random random = new Random();
        while (i < 3) {
            int number = random.nextInt(10);
            strNumber += number;
            i++;
        }
        return strNumber;
    }

    /**
     * Метод форматирует цифры для корректного вывода номера машины - перед двухзначным числом добавляет ноль,
     * перед однозначным - добавляет два ноля
     * Возвращает трехзначное число
     */
    private String formatNumber(String number) {
        String numberFinal;
        int format = Integer.parseInt(number);
        if (format < 999) {
            format++;
            numberFinal = Integer.toString(format);
            if (numberFinal.length() == 1) {
                numberFinal = "00" + numberFinal;
            }
            if (numberFinal.length() == 2) {
                numberFinal = "0" + numberFinal;
            }
            return numberFinal;
        } else {
            numberFinal = "000";
        }
        return numberFinal;
    }

    /**
     * Метод выполняет замену значения последней буквы следующим по порядку значением в массиве letters
     * В случае, если значение третей буквы эквивалентно последнему элементу в массиве, тогда вызывается
     * метод formatSecondLetter для перебора второй буквы, а третей букве присваивается значение первого элемента массива
     */
    private String formatThirdLetter(String letter) {
        String strLetter = letter.substring(2);
        int position = Arrays.asList(letters).indexOf(strLetter);
        String formatString = "";
        String newString;

        if (position == letters.length - 1) {
            formatString = formatSecondLetter(letter);
            newString = letters[0];
            return formatString.replace(strLetter, newString);
        } else {
            newString = letters[position + 1];
        }
       return letter.replace(strLetter, newString);
    }

    /**
     * Метод выполняет замену значения второй с конца буквы следующим по порядку значением в массиве letters
     * В случае, если значение второй буквы эквивалентно последнему элементу в массиве, тогда вызывается
     * метод formatFirstLetter для перебора первой буквы, а второй букве присваивается значение первого элемента массива
     */
    private String formatSecondLetter(String letter) {
        String strLetter = letter.substring(1,2);
        String formatString = "";
        String newString;

        int position = Arrays.asList(letters).indexOf(strLetter);
        if (position == letters.length-1) {
            formatString = formatFirstLetter(letter);
            newString = letters[0];
            return formatString.replace(strLetter, newString);
        } else {
            strLetter = letters[position + 1];
        }
        return letter.replace(letter.substring(1,2), strLetter);
    }

    /**
     * Метод выполняет замену значения первой буквы следующим значением по порядку в массиве letters
     * В случае, если значение первой буквы эквивалентно последнему элементу в массиве, тогда доступные
     * комбинации закончились.
     **/
    private String formatFirstLetter(String letter) {
        String strLetter = letter.substring(0,1);

        int position = Arrays.asList(letters).indexOf(strLetter);
        if (position == letters.length) {
            System.out.println("Нет доступных номеров");
        } else {
            strLetter = letters[position + 1];
        }
        return letter.replace(letter.substring(0, 1), strLetter);
    }
}
