package ru.barysheva.generator.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberServiceTest {

    @Test
    void random() {
        NumberService numberService = new NumberService();
        String randomNumber1 = numberService.random();
        String randomNumber2 = numberService.random();

        assertNotEquals(randomNumber1, randomNumber2);
    }

    @Test
    void next() {
        NumberService numberService = new NumberService();
        int nextNumber1 = Integer.parseInt(numberService.next().substring(1,4));
        int nextNumber2 = Integer.parseInt(numberService.next().substring(1,4));
        assertEquals(nextNumber1 + 1 , nextNumber2);
    }
}